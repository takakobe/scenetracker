#pragma once
#include "opencv2/opencv.hpp"
#include <iostream>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

struct SceneTrackParameter
{
	int LOG_NUM_MAX; // 記録するログの最大数
	int NUM_TRAILS; //中心点の軌跡を何フレーム分出すか
	int CORNER_COUNT; //トラッキングに使う点数

	// 既定値
	SceneTrackParameter()
	{
		LOG_NUM_MAX = 50;
		NUM_TRAILS = 50;
		CORNER_COUNT = 500;
	}


};

namespace sceneTrack
{
	// 前フレームの点に対応する現在フレームの点を得る
	cv::Point2f GetNextPointFromPrevPoint(const cv::Point2f& prev_pt, const cv::Mat& prev_img, const cv::Mat& next_img, const SceneTrackParameter& par = SceneTrackParameter());
	// 現在フレームの点に対応する前フレームの点を得る
	cv::Point2f GetPrevPointFromNextPoint(const cv::Point2f& next_pt, const cv::Mat& prev_img, const cv::Mat& next_img, const SceneTrackParameter& par = SceneTrackParameter());
	// 射影変換行列(homography)による射影先の点を返す．
	cv::Point2f GetDstPoint(const cv::Point2f& pt, const cv::Mat& homography);
	// 射影変換行列(homography)による射影元の点を返す．
	cv::Point2f GetSrcPoint(const cv::Point2f& pt, const cv::Mat& homography);
	// 前フレームから現在フレームの射影変換行列（Mat）を計算
	cv::Mat CalcHomography(const cv::Mat& prev_img, const cv::Mat& next_img, const SceneTrackParameter& par = SceneTrackParameter());
	// 射影変換行列を乗算
	cv::Mat MulHomography(const cv::Mat& prev_homo, const cv::Mat& next_homo);
	// 軌跡をimgに描画
	void DrawTrails(cv::Mat& img, const std::deque<cv::Mat>& homographies);
};

//
//class SceneTracker
//{
//
//public:
//	// デフォルトコンストラクタ
//	SceneTracker(void);
//	// デストラクタ
//	virtual ~SceneTracker(void);
//
//	// 基本インタフェース
//public:
//	// パラメータをセット
//	void SetParameter(const SceneTrackParameter& par){this->mParameter = par;}
//	// 画像を入力（画像はコピーされる）
//	void InputImage(const cv::Mat& img);
//	// 画像を入力（画像はコピーされないので，これを使用する際は，入力元の画像を変更してはいけない．
//	// しかし，コピーをしない分処理が少し速くなる
//	void InputStaticImage(cv::Mat& img);
//	// 射影変換行列を計算
//	cv::Mat CalcHomography(void);
//
//
//	cv::Mat GetHomography(void); // 射影変換行列を取得
//	cv::Mat GetAccumulatedHomography(void); // 蓄積された射影変換行列を取得
//	void StartAccumulateHomography(void); // 蓄積を開始
//	void StopAccumulateHomography(void); // 蓄積を停止
//	void ResetAccumulatedHomography(void); // 蓄積をリセット
//
//	cv::Point2f ProjectPoint(cv::Point2f pt); // 蓄積された射影変換行列を使って点を射影
//	
//	// 軌跡をimgに描画
//	void DrawTrails(cv::Mat& img);
//
//private:
//	SceneTrackParameter mParameter; // パラメータ
//	cv::Mat mInputImage; // 入力画像
//	cv::Mat mPrevImage; // 前フレーム画像
//	cv::Mat mNextImage; // 次フレーム画像
//
//	// マルチスレッド系
//public:
//	// スレッドを開始する
//	void BeginThread(SceneTracker *sceneTracker, bool isAccumulate = false);
//	// スレッドを終了する
//	void EndThread();
//	//void SuspendThread();
//
//private:
//	boost::thread mThread; // マルチスレッド
//	void threadFunc(); // スレッド関数
//
//	boost::mutex mImageInputMutex; // 画像入力の際の排他制御
//	boost::mutex mImageSetMutex; // 画像を使用する際の排他制御
//	boost::condition_variable mImageSetCondition; // 入力画像があるかどうかの状態
//
//	boost::mutex mHomographyMutex;
//	//boost::mutex m
//
//
//	boost::mutex mAccumulationMutex, mMutexCalc, mMutexEnd;
//	boost::condition_variable mCondition;
//	bool mEndFlag;
//
//
//private:
//	
//
//
//	void accumulateHomography(cv::Mat homography);
//	cv::Mat m_homography;
//	cv::Mat m_accumulatedHomography;
//	bool mIsAccumulate;
//	bool mIsUpdated;
//};
