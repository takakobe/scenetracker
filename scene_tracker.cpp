#include "scene_tracker.h"
using namespace boost;

using namespace std;
using namespace cv;
//using namespace sceneTrack;
namespace st = sceneTrack;

/// SceneTrack
namespace sceneTrack
{

	cv::Point2f GetNextPointFromPrevPoint(const cv::Point2f& prev_pt, const cv::Mat& prev_img, const cv::Mat& next_img, const SceneTrackParameter& par)
	{
		Mat homography; // 射影変換行列格納先
		homography = CalcHomography(prev_img, next_img, par); // 射影変換行列の計算
		return GetDstPoint(prev_pt, homography);
	}

	cv::Point2f GetPrevPointFromNextPoint(const cv::Point2f& next_pt, const cv::Mat& prev_img, const cv::Mat& next_img, const SceneTrackParameter& par)
	{
		Mat homography; // 射影変換行列格納先
		homography = CalcHomography(prev_img, next_img, par); // 射影変換行列の計算
		return GetDstPoint(next_pt, homography);
	}

	cv::Point2f GetDstPoint(const cv::Point2f& pt, const cv::Mat& homography)
	{
		cv::Point2f new_point;
		Mat src, dst, homo;
		src = (Mat_<float>(3, 1) << pt.x, pt.y, 1.0F);
		//src = Mat(3, 1, CV_32FC1);
		dst = Mat(3, 1, CV_32F);
		homo = Mat(homography);
		dst = homo * src;
		new_point.x = dst.at<float>(0, 0) / dst.at<float>(2, 0);
		new_point.y = dst.at<float>(1, 0) / dst.at<float>(2, 0);

		return new_point;
	}

	cv::Point2f GetSrcPoint(const cv::Point2f& pt, const cv::Mat& homography)
	{
		cv::Point2f new_point;
		Mat src, dst, homo;
		dst = (Mat_<float>(3, 1) << pt.x, pt.y, 1.0F);
		src = Mat(3, 1, CV_32F);
		homo = Mat(homography);

		src = homo.inv() * dst;
		new_point.x = src.at<float>(0, 0) / src.at<float>(2, 0);
		new_point.y = src.at<float>(1, 0) / src.at<float>(2, 0);

		return new_point;
	}

	cv::Mat CalcHomography(const cv::Mat& prev_img, const cv::Mat& next_img, const SceneTrackParameter& par)
	{
		cv::Mat gray_prevImage, gray_nextImage;
		if (prev_img.channels() == 1)
			gray_prevImage = prev_img;
		else
			cv::cvtColor(prev_img, gray_prevImage, CV_RGB2GRAY);
		if (next_img.channels() == 1)
			gray_nextImage = next_img;
		else
			cv::cvtColor(next_img, gray_nextImage, CV_RGB2GRAY);

		CvMat *mat = cvCreateMat(3, 3, CV_32F); // 射影変換行列格納先
		char *status;
		int i, j;
		CvPoint2D32f *corners1, *corners2;
		CvTermCriteria criteria;
		IplImage *eig_img, *temp_img; // テンポラリ画像
		IplImage *prev_pyramid, *next_pyramid;
		//int window_size = 10;
		int corner_count = par.CORNER_COUNT;

		// (1)必要な構造体の確保
		IplImage ipl_gray_prevImage = gray_prevImage;
		eig_img = cvCreateImage(cvSize(gray_prevImage.cols, gray_prevImage.rows), IPL_DEPTH_32F, 1);
		temp_img = cvCreateImage(cvSize(gray_prevImage.cols, gray_prevImage.rows), IPL_DEPTH_32F, 1);
		corners1 = (CvPoint2D32f *)cvAlloc(corner_count * sizeof (CvPoint2D32f));
		corners2 = (CvPoint2D32f *)cvAlloc(corner_count * sizeof (CvPoint2D32f));
		prev_pyramid = cvCreateImage(cvSize(gray_prevImage.cols + 8, gray_prevImage.rows / 3), IPL_DEPTH_8U, 1);
		next_pyramid = cvCreateImage(cvSize(gray_prevImage.cols + 8, gray_prevImage.rows / 3), IPL_DEPTH_8U, 1);
		status = (char *)cvAlloc(corner_count);
		criteria = cvTermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 64, 0.01);

		// (2)疎な特徴点を検出
		cvGoodFeaturesToTrack(&ipl_gray_prevImage, eig_img, temp_img, corners1, &corner_count, 0.001, 5, NULL); // 画像内の鮮明なコーナーを検出
		//cvFindCornerSubPix(m_prevImage, corners1, corner_count, cvSize(window_size,window_size), cvSize(-1,-1), cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03)); // コーナー位置を高精度化する（処理時間がかかる）

		//// (3)オプティカルフローを計算
		IplImage ipl_gray_nextImage = gray_nextImage;
		cvCalcOpticalFlowPyrLK(&ipl_gray_prevImage, &ipl_gray_nextImage, prev_pyramid, next_pyramid, corners1, corners2, corner_count, cvSize(10, 10), 4, status, NULL, criteria, 0);

		//// (4)計算されたフローをもとに連続する2フレームから射影変換行列を算出
		unsigned pts_num = 0;

		for (i = 0; i < corner_count; i++)
		{
			if (status[i]){
				pts_num++;
			}
		}

		//cvFindHomographyに4点以上必要だから特徴点のペアが4点未満だとエラーが起こる?
		if (pts_num > 3){
			CvMat* prev_pts = cvCreateMat(2, pts_num, CV_32F);
			CvMat* next_pts = cvCreateMat(2, pts_num, CV_32F);

			for (i = 0, j = 0; i < corner_count; i++)
			{
				if (status[i]){
					cvmSet(prev_pts, 0, j, corners1[j].x);
					cvmSet(prev_pts, 1, j, corners1[j].y);
					cvmSet(next_pts, 0, j, corners2[j].x);
					cvmSet(next_pts, 1, j, corners2[j].y);
					j++;
				}
			}

			//cvFindHomography(prev_pts, next_pts, homography, CV_RANSAC);
			cvFindHomography(prev_pts, next_pts, mat, CV_LMEDS, 3);

			cvReleaseMat(&prev_pts);
			cvReleaseMat(&next_pts);
		}
		else mat = NULL; // 射影変換行列の計算に失敗するとNULLを返す

		cvReleaseImage(&eig_img);
		cvReleaseImage(&temp_img);
		cvReleaseImage(&prev_pyramid);
		cvReleaseImage(&next_pyramid);
		cvFree(&corners1);
		cvFree(&corners2);
		cvFree(&status);

		Mat homography = Mat(mat);
		return homography;
	}

	cv::Mat MulHomography(const cv::Mat& prev_homo, const cv::Mat& next_homo)
	{
		return next_homo * prev_homo;
	}

	void DrawTrails(cv::Mat& img, const std::deque<cv::Mat>& homographies)
	{
		vector<CvPoint> trails; // 軌跡を一時的に格納
		cv::Point2f pt;
		pt.x = float(img.cols / 2);
		pt.y = float(img.rows / 2);
		trails.push_back(Point2f(pt));

		for (int i = 0; i < (int)homographies.size(); i++)
		{
			pt = GetDstPoint(pt, homographies[i]);
			trails.push_back(cvPointFrom32f(pt));
		}

		// 描画
		for (int i = 1; i < (int)trails.size(); i++)
			line(img, trails[i], trails[i - 1], CV_RGB(255, 0, 0), 3);
	}
};

//
//SceneTracker::SceneTracker(void)
//{
//	mParameter = SceneTrackParameter(); // とりあえずデフォルト値
//	//addHomographyToLog(Mat::eye(3, 3, CV_32F));
//	ResetAccumulatedHomography();
//	m_homography = Mat::eye(3,3,CV_32F);
//	mIsUpdated = true;
//	mIsAccumulate = true;
//}
//
//SceneTracker::~SceneTracker(void)
//{
//}
//
//void SceneTracker::InputImage(const cv::Mat& img)
//{
//	boost::mutex::scoped_lock lock(mImageInputMutex);
//	mInputImage = img.clone();
//}
//
//void SceneTracker::InputStaticImage(cv::Mat& img)
//{
//	boost::mutex::scoped_lock lock(mImageInputMutex);
//	mInputImage = img;
//}
//
//cv::Mat SceneTracker::CalcHomography(void)
//{
//	// 射影変換行列の格納先
//	cv::Mat homography;
//
//	// 射影変換行列を計算
//	if(!mPrevImage.empty() && !mNextImage.empty())
//		homography = st::CalcHomography(mPrevImage, mNextImage, mParameter);
//	
//	return homography;
//}
//
//void SceneTracker::BeginThread(SceneTracker *sceneTracker, bool isAccumulate)
//{
//	mIsAccumulate = isAccumulate;
//
//	// スレッド開始
//	mThread = boost::thread(boost::bind(&SceneTracker::threadFunc, sceneTracker));
//}
//
//void SceneTracker::EndThread()
//{
//	// 終了命令を出す
//	mThread.interrupt();
//}
//
//
//
//void SceneTracker::threadFunc()
//{
//	while(1)
//	{
//		// 入力画像を排他制御するためのロック
//		boost::mutex::scoped_lock lock(mImageSetMutex);
//		// 更新の必要があるまで待機
//		while(mIsUpdated)
//			mImageSetCondition.wait(lock);
//
//		// 射影変換行列を計算
//		m_homography = CalcHomography();
//
//		mIsUpdated = true;
//
//		// 処理の終了を伝える
//		mImageSetCondition.notify_all();
//
//		// 射影変換行列を蓄積しておく
//		if(mIsAccumulate)
//			accumulateHomography(m_homography);
//
//		// 中断ポイント
//		boost::this_thread::interruption_point();
//	}
//}
//
//void SceneTracker::accumulateHomography(Mat homography)
//{
//	mutex::scoped_lock look(mAccumulationMutex); // ロックをかける
//	if(mIsAccumulate)
//		m_accumulatedHomography = homography * m_accumulatedHomography;
//}
//
//Mat SceneTracker::GetHomography(void)
//{
//	unique_lock<boost::mutex> lock(mMutexCalc);
//	while(!mIsUpdated)
//		mCondition.wait(lock);
//	Mat homography;
//	mutex::scoped_lock look(mAccumulationMutex); // ロックをかける
//	homography = m_homography.clone();
//	mCondition.notify_all();
//	return homography;
//}
//
//Mat SceneTracker::GetAccumulatedHomography(void)
//{
//	unique_lock<boost::mutex> lock(mMutexCalc);
//	while(!mIsUpdated)
//		mCondition.wait(lock);
//	Mat homography;
//	mutex::scoped_lock look(mAccumulationMutex); // ロックをかける
//	homography = m_accumulatedHomography.clone();
//	mCondition.notify_all();
//	return homography;
//}
//
//
//void SceneTracker::ResetAccumulatedHomography(void)
//{
//	mutex::scoped_lock look(mAccumulationMutex); // ロックをかける
//	m_accumulatedHomography = Mat::eye(3,3,CV_32F);
//}
//
//void SceneTracker::StartAccumulateHomography(void)
//{
//	mutex::scoped_lock look(mAccumulationMutex); // ロックをかける
//	mIsAccumulate = true;
//	m_accumulatedHomography = Mat::eye(3,3,CV_32F);
//}
//
//void SceneTracker::StopAccumulateHomography(void)
//{
//	mutex::scoped_lock look(mAccumulationMutex); // ロックをかける
//	mIsAccumulate = false;
//}
//
//cv::Point2f SceneTracker::ProjectPoint(cv::Point2f pt)
//{
//	unique_lock<boost::mutex> lock(mMutexCalc);
//	while(!mIsUpdated)
//		mCondition.wait(lock);
//	Mat homography;
//	mutex::scoped_lock look(mAccumulationMutex); // ロックをかける
//	homography = m_accumulatedHomography;
//	mCondition.notify_all();
//	return st::GetDstPoint(pt, homography);
//}
//
//void SceneTracker::InputImage(cv::Mat& img)
//{
//	unique_lock<boost::mutex> lock(mMutexCalc);
//	while(!mIsUpdated)
//		mCondition.wait(lock);
//
//	// 現在フレームを保存しておく
//	if(mPrevImage) cvReleaseImage(&mPrevImage);
//	mPrevImage = mNextImage; 
//	// 画像を取得しモノクロ画像に変換
//	mNextImage = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1); // 画像用にメモリ確保
//	cvCvtColor(img, mNextImage, CV_BGR2GRAY);
//	mIsUpdated = false;
//	mCondition.notify_all();
//}
//
//void SceneTracker::CalcHomography(void)
//{
//	unique_lock<boost::mutex> lock(mMutexCalc);
//	while(mIsUpdated)
//		mCondition.wait(lock);
//	m_homography = Mat::eye(3,3,CV_32F);
//	if(mPrevImage)
//		m_homography = st::CalcHomography(mPrevImage, mNextImage, mParameter);
//	accumulateHomography(m_homography); // 射影変換行列を蓄積しておく
//	mIsUpdated = true;
//	mCondition.notify_all();
//}